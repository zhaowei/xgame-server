package com.xgame.service;

import com.xgame.mybatis.model.User;


public interface UserService {
//
	// 根据用户名查找用户
	public User findUserByName(String name) throws Exception;

	// 根据uId查找用户
	public User findUserByuId(Long uId);

	
}
