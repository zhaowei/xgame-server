package com.xgame.service;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xgame.data.OnlineUser;
import com.xgame.mybatis.mapper.UserMapper;
import com.xgame.mybatis.model.User;
import com.xgame.mybatis.model.UserExample;
import com.xgame.mybatis.model.UserExample.Criteria;


@Service
public class UserServiceImpl implements UserService {
	protected final Logger log = LoggerFactory.getLogger(getClass());
	
	
	@Autowired
	UserMapper userMapper;

	/**
	 * 根据账号查找用户
	 */
	public User findUserByName(String name) throws Exception {
		UserExample ex = new UserExample();
		Criteria c= ex.createCriteria();
		c.andUserNameEqualTo(name);
		List<User> list = this.userMapper.selectByExample(ex);
		User user = null;
		if (CollectionUtils.isNotEmpty(list)) {
			user = list.get(0);
		}
		return user;
	}
//
	/**
	 * 根据查找用户
	 */
	public User findUserByuId(Long uId) {
		User user = userMapper.selectByPrimaryKey(uId);
		return user;
	}
	
	/**
	 * 根据查找用户
	 */
	public User add(User user) {
		userMapper.insertSelective(user);
		return user;
	}
	
	void buyItem(OnlineUser onlineUser){
		
		System.out.println(new Throwable().getStackTrace()[0].getMethodName());
//		onlineUser.lock(this.getClass().getMethods()[0].getName());
//		onlineUser.
//		int oldCoin=1000;
		//扣钱
		
		//物品给到用户
		
	}
	
	public static void testGetClassName() {  
	    // 方法1：通过SecurityManager的保护方法getClassContext()
	    String clazzName = new SecurityManager() {  
	        public String getClassName() {  
	            return getClassContext()[1].getName();  
	        }  
	    }.getClassName();  
	    System.out.println(clazzName);  
	    // 方法2：通过Throwable的方法getStackTrace()  
	    String clazzName2 = new Throwable().getStackTrace()[1].getClassName();  
	    System.out.println(clazzName2);  
	    // 方法3：通过分析匿名类名称()  
	    String clazzName3 = new Object() {  
	        public String getClassName() {  
	            String clazzName = this.getClass().getName();  
	            return clazzName.substring(0, clazzName.lastIndexOf('$'));  
	        }  
	    }.getClassName();  
	    System.out.println(clazzName3);  
	    //方法4：通过Thread的方法getStackTrace()  
	    String clazzName4 = Thread.currentThread().getStackTrace()[2].getClassName();  
	    System.out.println(clazzName4);  
	}
	
//
//	/**
//	 * 保存日志
//	 */
//	public void saveLoginLog(Map<String, String> map, User user)
//			throws Exception {
//		UserLoginLog log = new UserLoginLog();
//		log.setUserId(user.getUid());
//		log.setUserName(user.getName());
//		log.setLoginTime(new Date());
//		log.setAppId(Integer.parseInt(map.get("appId")));
//		log.setAppId(Integer.parseInt(map.get("cpId")));
//		log.setAppId(Integer.parseInt(map.get("channelId")));
//		log.setAppId(Integer.parseInt(map.get("clientId")));
//		// userMapper.saveLoginLog(log);
//	}
//
//	public void saveUser(User user) throws Exception {
//		userMapper.insert(user);
//		throw new Exception("");
//	}
//
//	/**
//	 * 根据openId和类型查找用户
//	 */
//	public User findUserByOpenId(String openId, Integer type) {
//		Criteria example = new Criteria();
//		example.put("openId", openId);
//		example.put("type", type);
//		List<User> list = userMapper.selectByExample(example);
//		User user = null;
//		if (CollectionUtils.isNotEmpty(list)) {
//			user = list.get(0);
//		}
//		return user;
//	}
//
//	/**
//	 * 检查用户名是否存在
//	 * 
//	 * @throws Exception
//	 */
//	public boolean checkName(String name) throws Exception {
//		User user = findUserByName(name);
//		if (user != null) {
//			return true;
//		}
//		return false;
//	}
//
//	/**
//	 * 从cookie中拿到手机客户端信息，appId,clientId,channelId,cpId
//	 */
//	public Map<String, String> getCookie(HttpServletRequest request) {
//		Map<String, String> map = new HashMap<>();
//		Cookie[] cookie = request.getCookies();
//		for (Cookie c : cookie) {
//			if (c.getName().equals("info")) {
//				map = UserUtil.getCookieValue(c.getValue());
//			}
//		}
//		return map;
//	}
//
//	/**
//	 * 将手机信息保存到cookie
//	 */
//	public void addCookie(HttpServletRequest request,
//			HttpServletResponse response) {
//		String appId = request.getParameter("appId");
//		String channelId = request.getParameter("channelId");
//		String cpId = request.getParameter("cpId");
//		String clientId = request.getParameter("clientId");
//		// 需按顺序
//		String info = "" + appId + "&" + channelId + "&" + cpId + "&"
//				+ clientId;
//		Cookie cookie = new Cookie("info", info);
//		cookie.setMaxAge(60 * 60 * 24 * 7);
//		response.addCookie(cookie);
//	}
//
//	public int countByExample(Criteria example) {
//		int count = this.userMapper.countByExample(example);
//		log.debug("count: {}", count);
//		return count;
//	}
//
//	public User selectByPrimaryKey(Long uid) {
//		return this.userMapper.selectByPrimaryKey(uid);
//	}
//
//	@Override
//	public List<User> selectByExample(Criteria example) {
//		return this.userMapper.selectByExample(example);
//	}
//
//	public int updateByPrimaryKey(User record) {
//		return this.userMapper.updateByPrimaryKey(record);
//	}
//
//	public int insert(User record) {
//		return this.userMapper.insert(record);
//	}
//
//	/**
//	 * 快速注册
//	 * 
//	 * @author xiaoqingfeng
//	 */
//	@Override
//	public int insertfast(User user) {
//		// TODO Auto-generated method stub
//		int uid;
//		try {
//			// 用户注册
//			uid = this.userMapper.fastinsert(user);
//			if (uid == 1) {
//				// 积分注册
//				scoreService.insertUserScoreFirst(user);
//				// 奖品关联
//				if (user.getClientId() == null) {
//					return 1;
//				}
//				UserGoods userGoods = new UserGoods();
//				userGoods.setUserId(user.getUid());
//				userGoods.setClientId(user.getClientId());
//				userGoodsService.updateByPrimaryclientId(userGoods);
//			} else {
//				new Exception("插入数据失败");
//			}
//			return uid;
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	/**
//	 * 判断用户名是否存在
//	 * 
//	 * @author xiaoqingfeng
//	 */
//	@Override
//	public User queryByUserName(String name) {
//		// TODO Auto-generated method stub
//		return this.userMapper.queryByUserName(name);
//	}
//
//	@Override
//	public int insertThirdLogin(User user) {
//		// TODO Auto-generated method stub
//		return this.userMapper.insertThirdLogin(user);
//	}
//
//	@Override
//	public int updateByPrimaryKeySelective(User user) {
//		// TODO Auto-generated method stub
//		if (null != user) {
//			int result = this.userMapper.updateByPrimaryKeySelective(user);
//			return result;
//		}
//		{
//			return 0;
//		}
//	}
//
//	@Override
//	public int updateByExample(User user, ModifyPassword request) {
//		// TODO Auto-generated method stub
//		try {
//			user.setPassword(DigestUtils.md5Hex(request.getBody()
//					.getNewPassword()));
//			Map<String, Object> condition = new HashMap<String, Object>();
//			condition.put("password",
//					DigestUtils.md5Hex(request.getBody().getOldPassword()));
//			condition.put("uid", request.getBody().getUid());
//			int updateNumber = this.userMapper.updateByExampleSelective(user,
//					condition);
//			return updateNumber;
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	/**
//	 * 查询clientID最近登录的用户
//	 */
//	public User selectUserByClientId(ClientMoblieUser clientMoblieUser) {
//		Criteria example = new Criteria();
//		example.put("clientId", clientMoblieUser.getId());
//		example.setMysqlLength(2);
//		example.setMysqlOffset(0);
//		example.setOrderByClause("createTime");
//		List<User> userList = userMapper.selectByExample(example);
//		if (userList != null && userList.size() > 0) {
//			return userList.get(0);
//		} else {
//			return null;
//		}
//	}
	public static void main(String[] args) {
		testGetFunctionName();
//		new UserServiceImpl().buyItem(null);
//		String str ="/sns/static/img/delete/t1.jpg";
//		System.out.println(str.endsWith("delete/t1.jpg"));
	}
	public static void testGetFunctionName() {  
        // 方法1：通过Throwable的方法getStackTrace()  
        String funcName2 = new Throwable().getStackTrace()[1].getClassName();  
        System.out.println(funcName2);  
        //方法2：通过Thread的方法getStackTrace()  
        String clazzName4 = Thread.currentThread().getStackTrace()[2].getMethodName();  
        System.out.println(clazzName4);   
        
     // 方法3：通过分析匿名类名称()  
        String clazzName3 = new Object() {  
            public String getClassName() {  
                String clazzName = this.getClass().getName();  
                return clazzName.substring(0, clazzName.lastIndexOf('$'));  
            }  
        }.getClassName();  
        System.out.println(clazzName3);  
        
    } 
}
