package com.xgame.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.stereotype.Service;
@Service
public class CacheService {
	protected final Logger log = LoggerFactory.getLogger(getClass());
	
	SimpleCacheManager simpleCacheManager;
	
	public SimpleCacheManager getSimpleCacheManager() {
		return simpleCacheManager;
	}

	public void setSimpleCacheManager(SimpleCacheManager simpleCacheManager) {
		this.simpleCacheManager = simpleCacheManager;
	}

	public void flushSimpleCache(String cacheName){
		Cache cache=simpleCacheManager.getCache(cacheName);
		log.info("当前cache:{}", simpleCacheManager.getCacheNames());
		cache.clear();
		log.info(cacheName+"已经清空");;
	}
}
