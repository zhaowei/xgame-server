package com.xgame.memcache;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.danga.MemCached.MemCachedClient;

/**
 * Memcache客户端实现 注意：所有被放入memcache中的对象必须序列化 即实现java.io.Serializable
 * 
 * @author xiaoqingfeng
 */
@Component("memcache")
public class CacheClientImpl implements ICacheClient
{
	MemCachedClient cacheProvider;

	@Override
	public boolean add(String key, Object value)
	{
		return this.cacheProvider.add(key, value);
	}

	@Override
	public boolean add(String key, Object value, long expiry)
	{
		return this.cacheProvider.add(key, value, new Date(expiry));
	}

	@Override
	public boolean add(String key, Object value, long expiry, Integer hashCode)
	{
		return this.cacheProvider.add(key, value, new Date(expiry), hashCode);
	}

	@Override
	public boolean add(String key, Object value, Integer hashCode)
	{
		return this.cacheProvider.add(key, value, hashCode);
	}

	@Override
	public boolean delete(String key)
	{
		return this.cacheProvider.delete(key);
	}

	@Override
	public Object get(String key)
	{
		return this.cacheProvider.get(key);
	}

	@Override
	public Object get(String key, Integer hashCode)
	{
		return this.cacheProvider.get(key, hashCode);
	}

	@Override
	public Object get(String key, Integer hashCode, boolean asString)
	{
		return this.cacheProvider.get(key, hashCode, asString);
	}

	@Override
	public boolean keyExists(String key)
	{
		return this.cacheProvider.keyExists(key);
	}

	@Override
	public boolean replace(String key, Object value)
	{
		return this.cacheProvider.replace(key, value);
	}

	@Override
	public boolean replace(String key, Object value, long expiry)
	{
		return this.cacheProvider.replace(key, value, new Date(expiry));
	}

	@Override
	public boolean replace(String key, Object value, long expiry,
			Integer hashCode)
	{
		return this.cacheProvider.replace(key, value, new Date(expiry),
				hashCode);
	}

	@Override
	public boolean replace(String key, Object value, Integer hashCode)
	{
		return this.cacheProvider.replace(key, value, hashCode);
	}

	@Override
	public boolean set(String key, Object value)
	{
		return this.cacheProvider.set(key, value);
	}

	@Override
	public boolean set(String key, Object value, long expiry)
	{
		return this.cacheProvider.set(key, value, new Date(expiry));
	}

	@Override
	public boolean set(String key, Object value, long expiry, Integer hashCode)
	{
		return this.cacheProvider.set(key, value, new Date(expiry), hashCode);
	}

	@Override
	public boolean set(String key, Object value, Integer hashCode)
	{
		return this.cacheProvider.set(key, value, hashCode);
	}

	public MemCachedClient getCacheProvider()
	{
		return cacheProvider;
	}

	public void setCacheProvider(MemCachedClient cacheProvider)
	{
		this.cacheProvider = cacheProvider;
	}

	@Override
	public boolean add(String key, Object value, Date expiry) {
		// TODO Auto-generated method stub
		return cacheProvider.add(key, value, expiry);
	}

	@Override
	public boolean replace(String key, Object value, Date expiry) {
		// TODO Auto-generated method stub
		return  cacheProvider.replace(key, value, expiry);
	}
	
	@Override
	public boolean flushAll() {
		// TODO Auto-generated method stub
		return  cacheProvider.flushAll();
	}
	
	
}
