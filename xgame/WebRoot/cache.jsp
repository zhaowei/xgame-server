<%@page import="com.xgame.service.CacheService"%>
<%@page import="org.springframework.cache.Cache"%>
<%@page import="com.xgame.memcache.ICacheClient"%>
<%@page import="com.xgame.util.VM"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="../inc/taglib.jsp" %>
<html>
  <head>  
    <title>My JSP 'vm.jsp' starting page</title>    
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
  </head>
  <body>
<%
ApplicationContext context=WebApplicationContextUtils.getWebApplicationContext(getServletContext());
ICacheClient memcache=context.getBean(ICacheClient.class);
CacheService simpleCache=context.getBean(CacheService.class);
    String key = request.getParameter("key");
    if(key != null ){
    	if(key.equals("flushAll")){
    		memcache.flushAll();
    	}else{
	    	boolean isexist=memcache.keyExists(key);
		    out.println(key+"参数是否存在:"+isexist);
		    if(isexist){
		    	memcache.delete(key);
		    }
		    out.println(memcache.get("micMsgScore2"));
		    out.println("成功重新装载Cache.谢谢");
    	}
    }else{
    	 String sk = request.getParameter("sk");
    	if(sk!=null){
    		if(simpleCache.getSimpleCacheManager().getCache("sk")!=null){
	    		simpleCache.flushSimpleCache(sk);
		    	out.println("已经清空"+sk);
    		}else
	    		out.println("请输入正确的simple缓存名,谢谢");
    	}else
	    	out.println("请输入验证参数,谢谢");
    }
%>
</body>