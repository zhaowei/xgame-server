<%@page
	import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	StringBuffer extInfos = new StringBuffer();
	if (request.getAttribute("__conf") == null) {
		request.setAttribute("ctx", request.getContextPath());
		request.setAttribute("__conf", Boolean.TRUE);
		StringBuffer formInfos = new StringBuffer();
	}
%>
<script>
	_ = {
		server : {
			userServer : '${vm.userServer}',
			staticServer : '${vm.staticServer}'
		},
		user : {
			name: '${USER != null ? USER.name : "游客"}',
			uid: ${USER != null ? USER.uid : 0},
			isLogin : ${USER != null ? USER.login : false},
		    role:  ${USER != null ? USER.role : 3},
			vip : ${USER != null ? (USEREXTINFO.role == null ? false : (USEREXTINFO.role==0)?false:true) : false},
			right : {
				post : true
			}
		},
		client : {

		}
}
</script>