<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.xgame.controller.exception.StringPrintWriter"%>
<%@ page language="java" contentType="text/html; charset=GBK"
    pageEncoding="utf-8"%>
<%@ page import="java.lang.Exception"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, target-densitydpi=high-dpi, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="keywords" content="异常__游乐社区">
<link rel="bookmark" href="http://test.static.y6game.cn/sns/static/img/favicon.ico" type="image/x-icon" /> 
<link rel="icon" href="http://test.static.y6game.cn/sns/static/img/favicon.ico" type="image/x-icon" /> 
<link rel="shortcut icon" href="http://test.static.y6game.cn/sns/static/img/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="http://test.static.y6game.cn/sns/static/css/reset.css" />
<link rel="stylesheet" href="http://test.static.y6game.cn/sns/static/css/common.css" />
<link rel="stylesheet" href="http://test.static.y6game.cn/sns/static/css/error.css" />
<title>异常__游乐社区</title>
<head>
<body>
<header class="header">
	<ul class="navigation">
		<li class="item"><a class="block" href="/sns/activity/"><span class="title">活动</span></a></li>
		<li class="item"><a class="block" href="/sns/hub/"><span class="title">广场</span></a></li>
		<li class="item"><a class="block" href="/page/sns/index"><span class="title">推荐</span></a></li>
		<li class="item"><a class="block" href="/sns/today/"><span class="title">今日</span></a></li>
		<li class="item"><a class="block" href="/sns/i/"><span class="title">&nbsp;我&nbsp;</span></a></li>
	</ul>
</header>
<div class="wrapper" id="error">
	<div class="syserror">
		<p class="message">抱歉，你要找的资源查找不到</p>
	</div>
	<div class="quick-link">
		<a href="#" onclick="javascript: history.back(); return false">返回上一页</a>
		&nbsp;&nbsp;|&nbsp;&nbsp;
		<a href="/">回到Y6主页</a>
		&nbsp;&nbsp;|&nbsp;&nbsp;
		<a href="/page/user/my">去我的主页</a>
	</div>
	<div class="error-detail j-detail">
		<h3>错误详情</h3>
		<i class="icon-dropdown j-icon"></i>
		<i class="icon-dropup j-icon"></i>
		<p>
		<img alt="" src="http://res.y6.cn/img/404.gif">
		</p>
	</div>
</div>
<script src="http://test.static.y6game.cn/sns/static/js/jquery.min.js"></script>
<script src="http://test.static.y6game.cn/sns/static/js/sea.js"></script>
<script src="http://test.static.y6game.cn/sns/config/config.js"></script>
<script>
	$('#error').find('.j-icon').click(function(){
		$(this).closest('.j-detail').toggleClass('press');
	})
</script>
<script src="http://s9.cnzz.com/stat.php?id=5380208&web_id=5380208" type="text/javascript"></script>
</body>
</html>