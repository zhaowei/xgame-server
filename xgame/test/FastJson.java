import java.lang.reflect.Type;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.serializer.JSONLibDataFormatSerializer;
import com.alibaba.fastjson.serializer.JSONSerializerMap;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
import com.alibaba.fastjson.util.DeserializeBeanInfo;

public class FastJson {

	private static final SerializeConfig config;
//	private static final ParserConfig pconfig;
	static {
		config = new SerializeConfig();
		config.put(java.util.Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd hh:mm:ss")); // 使用和json-lib兼容的日期输出格式
//		config.put(java.sql.Date.class, new JSONLibDataFormatSerializer()); // 使用和json-lib兼容的日期输出格式
		
//		pconfig.putDeserializer(java.util.Date.class);
	}

	private static final SerializerFeature[] features = {
//			SerializerFeature.WriteMapNullValue, // 输出空置字段
			SerializerFeature.WriteNullListAsEmpty, // list字段如果为null，输出为[]，而不是null
			SerializerFeature.WriteNullNumberAsZero, // 数值字段如果为null，输出为0，而不是null
			SerializerFeature.WriteNullBooleanAsFalse, // Boolean字段如果为null，输出为false，而不是null
			SerializerFeature.WriteNullStringAsEmpty // 字符类型字段如果为null，输出为""，而不是null
	};
	
	private static final Feature[] features2 = {
//			SerializerFeature.WriteMapNullValue, // 输出空置字段
		Feature.InitStringFieldAsEmpty,
	};

	// 序列化为和JSON-LIB兼容的字符串
	public static String toCompatibleJSONString(Object object) {
		return JSON.toJSONString(object, config, features);
	}
	// 序列化为和JSON-LIB兼容的字符串
	@SuppressWarnings("unchecked")
	public static <T> T toCompatibleObj(String object,Class<?> t) {
		return (T) JSON.parseObject(object,t,features2);
	}
	
}