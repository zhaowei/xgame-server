import java.util.Date;

import org.junit.Test;

import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
import com.xgame.mybatis.model.User;


public class TestFastJson {
	private static SerializeConfig mapping = new SerializeConfig();
	    static {
	        mapping.put(Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd"));
	    }
	    
	    @Test
	    public void test_0() {
	        try {
				Date date = new Date();
				User user=new User();
				user.setAddress("aa");
				String text = FastJson.toCompatibleJSONString(user);
				System.out.println(text);
//	        Assert.assertEquals(JSON.toJSONString(new SimpleDateFormat("yyyy-MM-dd").format(date)), text);
				User user1=FastJson.toCompatibleObj(text,User.class);
				System.out.println(user1.getAddress());
//				System.out.println(user1.getCreatetime());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    
}
