package com.xgame.controller.pb;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.google.protobuf.spring.http.ProtobufHttpMessageConverter;
import com.xgame.poto.UserProto.UserPb;

/**
 * 
 * @author shannon
 */
public class PBClientTest {
	private final RestTemplate restTemplate = new RestTemplate();
	
	public  void test(){
		String url = "http://localhost:8080/x/test/connected";
		List<HttpMessageConverter<?>> messageConverters=new ArrayList<>();
//		messageConverters.add(new PBMessageConverter());
		messageConverters.add(new ProtobufHttpMessageConverter());
		restTemplate.setMessageConverters(messageConverters);
		HttpHeaders headers = new HttpHeaders();
		StringBuilder a=new StringBuilder();
		a.append("cc");
		for (int i = 0; i < 100000; i++) {
			a.append("aa"+i);
		}
		System.out.println("字符串长"+a.toString().getBytes().length);
		UserPb userPb=UserPb.newBuilder().setUserName("赵巍").setEmail(a.toString()).setId(1).build();
//		MediaType mediaType=new MediaType("application", "x-protobuf", Charset.forName("UTF-8"));
//		MediaType mediaType=new MediaType("x", "pb");
//		MediaType mediaType=new MediaType("application", "json");
//		headers.setContentType(mediaType);
//		headers.set("Accept","x/pb");
	    HttpEntity<UserPb> request= new HttpEntity<UserPb>(userPb, headers);
	    System.out.println("accept"+request.getHeaders().get("Accept"));;
	    System.out.println("head"+request.getHeaders());;
		Long begin=System.currentTimeMillis();
//		for (int i = 0; i < 1000; i++) {
		UserPb userPb2= restTemplate.postForObject(url, request, UserPb.class);
			System.out.println("服务端返回"+userPb2.getUserName());
//		}
		Long end=System.currentTimeMillis();
		System.out.println("请求处理时间："+(end-begin));
		System.out.println("header大小："+request.getHeaders().size());
		System.out.println("request序列化后的大小："+request.getBody().getSerializedSize());
		System.out.println("初始化失败信息："+request.getBody().getInitializationErrorString());
	}
	
	public static void main(String[] args) throws IOException {
		
		new PBClientTest().test();
//		String url = "http://localhost:8080/s/pbtest/upload.pb";
//		upload(url);
//		String url2 = "http://www.example.com/pbtest/download.pb";
//		download(url2);
	}

	public static void upload(String url) throws IOException {
//		Person john = Person
//				.newBuilder()
//				.setId(1234)
//				.setName("John Doe")
//				.setEmail("jdoe@example.com")
//				.addPhone(
//						Person.PhoneNumber.newBuilder().setNumber("555-4321")
//								.setType(Person.PhoneType.HOME)).build();
//		AddressBook addressBook = AddressBook.newBuilder().addPerson(john)
//				.build();
//		byte[] content = addressBook.toByteArray();
//
//		URL targetUrl = new URL(url);
//		HttpURLConnection connection = (HttpURLConnection) targetUrl
//				.openConnection();
//		connection.setDoOutput(true);
//		connection.setDoInput(true);
//		connection.setRequestProperty("Content-Type", "application/x-protobuf");
//		connection.setRequestProperty("Accept", "application/x-protobuf");
//		connection.setRequestMethod("POST");
//		connection.setRequestProperty("Connect-Length",
//				Integer.toString(content.length));
//		connection.setFixedLengthStreamingMode(content.length);
//		OutputStream outputStream = connection.getOutputStream();
//		outputStream.write(content);
//		outputStream.flush();
//		outputStream.close();
	}

	public static void download(String url) throws IOException {
//		URL target = new URL(url);
//		HttpURLConnection conn = (HttpURLConnection) target.openConnection();
//		conn.setDoOutput(true);
//		conn.setDoInput(true);
//		conn.setRequestMethod("GET");
//		conn.setRequestProperty("Content-Type", "application/x-protobuf");
//		conn.setRequestProperty("Accept", "application/x-protobuf");
//		conn.connect();
//		// check response code
//		int code = conn.getResponseCode();
//		System.out.println("code:" + code);
//		System.out.println(conn.getContent());
//		boolean success = (code >= 200) && (code < 300);
//
//		InputStream in = success ? conn.getInputStream() : conn
//				.getErrorStream();
//		AddressBook addressBook = AddressBook.parseFrom(in);
//		in.close();
//		System.out.println(addressBook);
	}
}
