package com.xgame.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.alibaba.fastjson.JSON;
import com.xgame.controller.interceptor.LoggedInterceptor;
import com.xgame.filter.LogContextFilter;
import com.xgame.mybatis.model.User;
import com.xgame.poto.UserProto.UserPb;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath:spring/application-mail.xml",
		"classpath:spring/spring*.xml", })
// classpath:spring/spring-db-user.xml,
// classpath:spring/spring-db-log.xml,
// classpath:spring/spring-common.xml
// spring-mvc.xml
public class ControllerTest {
	protected final Logger log = LoggerFactory.getLogger(getClass());
	// @Autowired
	// private UserController userController;//你要测试的Controller
	@Autowired
	private TestController demoController;// 你要测试的Controller

	@Autowired
	private LoggedInterceptor loggedInterceptor;

	private LogContextFilter logContextFilter = new LogContextFilter();

	private MockMvc mockMvc;

	 @Before
	public void setup() {
		try {
			logContextFilter.setIncludePayload(true);
			logContextFilter.setIncludeClientInfo(true);
			logContextFilter.setMaxPayloadLength(200);
			mockMvc = MockMvcBuilders.standaloneSetup(demoController)
					.addFilters(logContextFilter)
					.addInterceptors(loggedInterceptor).build();
			log.debug("Hello {}", "debug message");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//	 @Test
	public void testUsers() throws Exception {
		Map<Object, Object> map = new HashMap<>();
		map.put("openId", "asdasdasd");
		map.put("type", "22");
		User user = new User();
		user.setUserName("dd");
		user.setCreateTime(new Date());
		map.put("user", user);
		String a = JSON.toJSONString(map, true);
		log.debug(a);
		ResultActions ra = this.mockMvc.perform(MockMvcRequestBuilders
				.post("/test/login").content(a).contentType(MediaType.APPLICATION_JSON)
				.param("page", "1").param("limit", "10"));
		MvcResult mr = ra.andReturn();
		System.out.println(mr.getResolvedException());
		String result = mr.getResponse().getContentAsString();
		log.debug("resp" + result);
	}

	@Test
	public void testPb() throws Exception {
		try {
			UserPb userPb=UserPb.newBuilder().setUserName("zw222").setEmail("zw@y6").setId(1).build();
			log.debug(userPb.toString());
			MediaType mediaType=new MediaType("application","x-protobuf");
			
			byte[] content = userPb.toByteArray();
			ResultActions ra = this.mockMvc.perform(MockMvcRequestBuilders
					.post("/test/connected").characterEncoding("UTF-8").contentType(mediaType).content(content)
					.param("page", "1"));
//		ResultActions ra = this.mockMvc.perform(MockMvcRequestBuilders
//				.post("/test/login").contentType(MediaType.APPLICATION_JSON)
//				.accept(MediaType.APPLICATION_JSON).content(a)
//				.param("page", "1").param("limit", "10"));
			MvcResult mr = ra.andReturn();
			System.out.println(mr.getResolvedException());
			String result = mr.getResponse().getContentAsString();
			log.debug("resp" + result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	 
//	@Test
	public static void httpTest() {
		try {
			System.out.println("testHttp 开始");
			URL target = new URL("http://localhost:8080/x/test/connected");
			HttpURLConnection conn = (HttpURLConnection) target
					.openConnection();
			UserPb userPb=UserPb.newBuilder().setUserName("zw222").setEmail("zw").setId(1).build();
			conn.setConnectTimeout(10*1000);
			byte[] content = userPb.toByteArray();
			conn.setRequestProperty("Content-Type", "x/pb");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connect-Length",
					Integer.toString(content.length));
			conn.setFixedLengthStreamingMode(content.length);
			OutputStream outputStream = conn.getOutputStream();
			outputStream.write(content);
			outputStream.flush();
			outputStream.close();
			//--
			int code = conn.getResponseCode();
			System.out.println("长度："+conn.getContentLength());
			System.out.println("code:" + code);
			boolean success = (code >= 200) && (code < 300);
			InputStream in = success ? conn.getInputStream() : conn
					.getErrorStream();
			System.out.println(in.toString());
			UserPb user = UserPb.parseFrom(conn.getInputStream());
			System.out.println(user);
			in.close();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		httpTest();
	}
	// @Test
	// public void testHome() throws Exception {
	// mockMvc.perform(get('/'))
	// .andExpect(status().isOk())
	// .andExpect(forwardedUrl('WEB-INF/pages/index.jsp'));
	// }
	// @Test
	// public void testPersistenceStatus() throws Exception {
	// mockMvc.perform(get('/roundtrip'))
	// .andExpect(status().isOk())
	// .andExpect(forwardedUrl('WEB-INF/pages/roundtrip.jsp'))
	// .andExpect(model().attributeExists('RoundTrip'));
}
