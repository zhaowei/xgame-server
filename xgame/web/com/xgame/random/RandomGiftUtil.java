package com.xgame.random;
//package com.xgame.controller.util;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//
//import com.xgame.controller.data.sns.GiftRspBody;
//import com.xgame.pojo.Goods;
//import com.xgame.service.GoodsService;
//import com.xgame.util.goods.Gift;
//import com.xgame.util.goods.LotteryUtil;
//
//public class RandomGiftUtil {
//
//	/**
//	 * 抽奖工具类
//	 */
//
//	public static Goods randomGift(List<Goods> goodsList) {
//
//		List<Double> orignalRates = new ArrayList<Double>(goodsList.size());
//		for (Goods gift : goodsList) {
//			double probability = gift.getProbability();
//			if (probability < 0) {
//				probability = 0;
//			}
//			orignalRates.add(probability);
//		}
//		int j = LotteryUtil.lottery(orignalRates);
//		return goodsList.get(j);
//	}
//
//	/**
//	 * 装配body
//	 */
//	public static GiftRspBody makeGiftBody(Goods goods) {
//		GiftRspBody body = new GiftRspBody();
//		body.setGiftId(goods.getId());
//		body.setGiftName(goods.getName());
//		
//		if (goods.getType() == 0) {
//			body.setMsg("手气有些不好没有抽到奖品，再试试") ;
//			body.setLucky(false);
//		}else if(goods.getType()==1 || goods.getType()==2){
//			body.setMsg("恭喜你获得" + goods.getName() + "快到你的仓库领取吧");
//			body.setLucky(true);
//		}else if(goods.getType()==3){
//			body.setMsg("虽难没抽到奖品但是你还有再抽一次的机会");
//			body.setLucky(true);
//		}
//		return body;
//	}
//
//}
