package com.xgame.random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

public class ErrorMessage implements HandlerExceptionResolver {
	private static Logger logger = LoggerFactory.getLogger(ErrorMessage.class);
	@Override
	public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
		logger.error("Catch Exception: ", ex);// 把漏网的异常信息记入日志
		return null;
	}
}