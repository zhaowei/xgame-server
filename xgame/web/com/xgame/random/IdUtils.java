package com.xgame.random;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 获取主键ID，使用时间+计数器方式实现
 * 以2013-06-13为基准
 * @author 
 */
public class IdUtils {

	public static IdUtils instance = null;

	private static final long ONE_STEP = 100;
	private static long lastTime = System.currentTimeMillis();
	private static short count = 0;

	/**
	 * 获取单例对象
	 * 
	 * @return
	 */
	public synchronized static IdUtils getInstanse() {
		if (instance == null) {
			instance = new IdUtils();
		}
		return instance;
	}

	private IdUtils() {
	}

	/**
	 * 根据对象获取表主键
	 * 
	 * @param clazz
	 * @return
	 */
	public synchronized String getUID() {
		try {
			if (count == ONE_STEP) {
				boolean done = false;
				while (!done) {
					long now = System.currentTimeMillis();
					if (now == lastTime) {
						try {
							Thread.sleep(1);
						} catch (java.lang.InterruptedException e) {
						}
						continue;
					} else {
						lastTime = now;
						count = 0;
						done = true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
		Long nowTime = null;
		   try {
			Date date = myFormatter.parse("2013-06-13");
		    nowTime = date.getTime() ;
		} catch (Exception e) {
			e.printStackTrace();
		}
		String result = (lastTime-nowTime)  + "" + (count++);
		return result;
	}
	
	
	
	
	public static void main(String[] args){
		for(int i=0;i<1000;i++){
			System.out.println(getInstanse().getUID()) ;
			try {
				Thread.sleep(100);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
// 使用时，只需调用IdUtils.getInstanse().getUID()即可，不会出现重复记录
