package com.xgame.random;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AccountRandomUtil {

	// 以时间戳生成游客账号
	public static String RandomAccount() throws ParseException {

		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = myFormatter.parse("2013-05-30");
		long time = new Date().getTime() - date.getTime();
		String account = "游客" + Integer.toHexString((int) time);
		return account;
	}

	public static void main(String[] args) throws ParseException, InterruptedException {
		for (int i = 0; i < 10000; i++) {
			String account = RandomAccount();
			System.out.println(account);
			Thread.sleep(10);
		}
	}

}
