package com.xgame.random;

import java.util.Random;

public class RandomUtil {

	/**
	 * 随机生成两个和小于10的数放到数组中，第三个数表示他们的和 服务端用ajax与客户端交互将随机 生成的两个数传个客户端自己保存第三个数,数字非0
	 * 当用户登录时验证 输出的格式为 data{ state{ code=1 msg="获去验证码成功" } body{ num1:1,num2:6 }
	 * } 验证码的图片样式由前端控制
	 * 
	 */
	public static int[] getRandomNum() {
		int[] num = new int[3];
		Random rand = new Random();
		num[0] = rand.nextInt(9);
		if (num[0] == 0) {
			num[0] = 1;
		}
		num[1] = rand.nextInt(10 - num[0]);
		if (num[1] == 0) {
			num[1] = 1;
		}
		num[2] = num[0] + num[1];
		return num;
	}

	public static void main(String[] args) throws InterruptedException {
		for (int j = 0; j < 1000000; j++) {
			System.out.println("***********test " + j + "***************");
			int[] num = getRandomNum();
			for (int m : num) {
				System.out.print(m + " ");
			}
		}
	}
}
