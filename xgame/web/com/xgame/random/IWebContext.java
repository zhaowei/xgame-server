package com.xgame.random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xgame.controller.type.UserTypeEnum;

/**
 * 用于实现上下文连接 用于在过滤器中实现注入Request与Response
 * @author Administrator
 *
 */
public interface IWebContext {
    /**
     * 设置请求与应答上下文
     * @param request 请求
     * @param response 应答
     * @param userType 用户类型
     * @param loginUrl 登录页面的URL
     */
    public void setWebContext(HttpServletRequest request, HttpServletResponse response, UserTypeEnum userType, String loginUrl);
    
//    /**
//     * 获取登录帐号
//     * @return 返回当前的登录帐号，如果没有登录则返回空
//     */
//    public LoginUser getLoginUser();
}