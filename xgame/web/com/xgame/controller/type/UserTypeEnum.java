package com.xgame.controller.type;


public enum UserTypeEnum {
	 	MY(0,"官方"),QQ(1, "qq用户"), SINA(2, "新浪用户");

	    private Integer id;

	    private String name;

	    private UserTypeEnum(Integer id, String name) {
	        this.id=id;
	        this.name=name;
	    }

	    public static UserTypeEnum getById(Integer id) {
	        if(null==id){
	            return null;
	        }
	        for(UserTypeEnum tmp: UserTypeEnum.values()) {
	            if(tmp.id.intValue() == id.intValue()) {
	                return tmp;
	            }
	        }
	        return null;
	    }

	    public Integer getId() {
	        return id;
	    }

	    public void setId(Integer id) {
	        this.id=id;
	    }

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name=name;
	    }

}
