package com.xgame.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xgame.controller.util.ClientUtil;
import com.xgame.mybatis.model.ClientMoblieUser;

@Controller
@RequestMapping("/client")
public class ClientController extends BaseController {

//	@Autowired
//	ScoreService scoreService;
//
	/**
	 * 进入登录平台接口
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/start")
	public String start(HttpServletRequest request, HttpServletResponse response) {
		if (log.isDebugEnabled()) {
			log.debug("******start**************");
		}
		try {
			ClientMoblieUser clientMoblieUser = new ClientMoblieUser();
			String M = getParam("M");
			if (StringUtils.isNotEmpty(M)) {
				ClientUtil.AnalysisUrl(M, clientMoblieUser);
				clientMoblieUser.setLastLoginIP(getRemoteIp());
				clientMoblieUser = clientService
						.addClientMobile(clientMoblieUser);
			}
			if (log.isDebugEnabled()) {
				log.debug("******start end**************");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "user/login/login";
	}
}
