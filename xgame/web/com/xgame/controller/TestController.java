package com.xgame.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xgame.poto.UserProto.UserPb;
import com.xgame.service.UserService;

@Controller
@RequestMapping("/test")
public class TestController extends BaseController{
	@Autowired
	UserService userService;
	
	/**
	 * 游乐用户登陆
	 */
	@RequestMapping(value = "/login")
	public @ResponseBody
	Map<String, Object> test(@RequestBody Map<Object, Object> data,
			HttpServletRequest request) {
		Map<String, Object> map=null;
		try {
			log.info("服务端收到openId:"+(String)data.get("openId"));
			log.info(""+(String)data.get("type"));
			map = new HashMap<String, Object>();
			map.put("retCode", 0);
			map.put("msg", "用户名不存在");
			map.put("sId", "");
			userService.findUserByName("test");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
//	
	
	@RequestMapping(value="/get",method = RequestMethod.GET)  
	@ResponseBody  
	public UserPb get() throws IOException {  
	    log.info("get进来了");
	    UserPb.Builder userPb = UserPb.newBuilder().setUserName("赵巍");  
//	    PhoneNumber value=PhoneNumber.newBuilder().setNumber("22").build();
//		userPb.addPhone(value);
		log.info(userPb.build().toString()); 
		return userPb.build(); 
	}  
	  
	@RequestMapping(value="/connected",method = RequestMethod.POST)  
	@ResponseBody  
	public UserPb connected(@RequestBody UserPb useClientInfo) throws IOException {  
//		log.info("服务端收到:"+useClientInfo.toString());
		log.info(useClientInfo.getUserName()+"进来了");
		UserPb.Builder userPb = UserPb.newBuilder().setId(1).setUserName("返回的名字ok");  
//		userPb.addPhone(value);
//		log.info(userPb.toString()); 
		return userPb.build(); 
	}  
	
//	@ResponseBody  
//	@RequestMapping("getuser")  
//	public UserPb getUseClientInfo(HttpServletResponse response) throws IOException{  
//		UserPb.Builder useClientInfoBuilder = UserPb.newBuilder();  
//		 log.info(useClientInfoBuilder.toString());  
////	    ……业务逻辑略……  
//	    return useClientInfoBuilder.build();  
//	}  
}
