package com.xgame.controller.data;



/**
 *客户端发送过来的信息 
 */
public class UserReq {

	private long id;
	private String sid;
	private String mac;
	private Info info;

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public class Info {
		String name;
		String password;
		Integer remember;
		Integer autoLogin;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public Integer getRemember() {
			return remember;
		}

		public void setRemember(Integer remember) {
			this.remember = remember;
		}

		public Integer getAutoLogin() {
			return autoLogin;
		}

		public void setAutoLogin(Integer autoLogin) {
			this.autoLogin = autoLogin;
		}

	}
}

