/**
 * 系统项目名称
 * com.ym.market.resource.data
 * Snippet.java
 * 
 * 2013-3-7-下午8:39:25
 *  2013指点公司-版权所有
 * 
 */
package com.xgame.controller.data;

/**
 * zwei
 * zwei
 * 2013-3-7 下午8:39:25
 * 
 * @version 1.0.0
 * 
 */
public class State{
	
	private Integer code=1;
	
	private String msg="ok";

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}

