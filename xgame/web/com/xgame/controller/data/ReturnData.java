package com.xgame.controller.data;

import org.slf4j.Logger;

public class ReturnData
{
	private Long id;
	
	private Long UID;
	
	private Long CID;
	
	private State state;
	
	/**
	 * 执行结果
	 */
	private Boolean success;
 
	/**
	 * 错误代码
	 */
	private Integer errCode;//301=重新登录 
	
	/**
	 * 返回总记录数
	 * */
	private Integer totalCnt;
	
	/**
	 * 是最后一条
	 */
	private Boolean isLast;

	public Long getUID() {
		return UID;
	}

	public void setUID(Long uID) {
		UID = uID;
	}

	public Long getCID() {
		return CID;
	}

	public void setCID(Long cID) {
		CID = cID;
	}

	public ReturnData() {
		// TODO Auto-generated constructor stub
		state=new State();
	}
	
	public ReturnData(Long id) {
		this.id=id;
	}
	
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
	
	public Boolean getIsLast() {
		return isLast;
	}

	public void setIsLast(Boolean isLast) {
		this.isLast = isLast;
	}

	public Integer getTotalCnt() {
		return totalCnt;
	}

	public void setTotalCnt(Integer totalCnt) {
		this.totalCnt = totalCnt;
	}

	public Boolean getSuccess()
	{
		return success;
	}

	public void setSuccess(Boolean success)
	{
		this.success = success;
	}

	public Integer getErrCode()
	{
		return errCode;
	}

	public void setErrCode(Integer errCode)
	{
		this.errCode = errCode;
	}

	public String toString()
	{
		return "success:" + success + "    errorCode:" + errCode;
	}
	
	public void setErrorResult(String msg) {
		this.success=false;
		this.state.setCode(0);
		this.state.setMsg(msg);
	}
	
	/**
	 * 带消息和日志的异常
	 * @param msg 友好提示错误消息
	 * @param log 日志组件
	 * @param e 异常消息
	 */
	public void setErrorResult(String msg,Logger log,Exception e) {
		this.state.setCode(0);
		this.state.setMsg(msg);
		log.error(e.getLocalizedMessage(),e);
	}
	
	public void setErrorResult(Logger log,Exception e) {
		this.state.setCode(0);
		this.state.setMsg(e.getLocalizedMessage());
		log.error(e.getLocalizedMessage(),e);
	}
	/**
	 * 提示信息
	 * @param log
	 * @param e
	 * @param pmsg
	 */
	public void setErrorResult(Logger log,Exception e,String pmsg) {
		this.state.setCode(0);
		this.state.setMsg(pmsg);
		log.error(e.getLocalizedMessage(),e);
	}
	
	public void setErrorResult(String msg,Integer errCode) {
		this.errCode=errCode;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
