//package com.xgame.controller;
//package com.xgame.controller;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import com.example.tutorial.AddressBookProtos.AddressBook;
//import com.example.tutorial.AddressBookProtos.Person;
//
///**
// *  * @author shannon  *  
// */
//@Controller
//@RequestMapping("/pbtest")
//public class PbTestController {
//	
//	@RequestMapping("upload")
//	public void upload(HttpServletRequest request, HttpServletResponse response)
//			throws IOException {
//		InputStream inputStream = request.getInputStream();
//		AddressBook addressBook = AddressBook.parseFrom(inputStream);
//		inputStream.close();
//		System.out.println(addressBook);
//	}
//
//	@RequestMapping("download")
//	public void download(HttpServletResponse response) throws IOException {
//		Person john = Person
//				.newBuilder()
//				.setId(1234)
//				.setName("John Doe")
//				.setEmail("jdoe@example.com")
//				.addPhone(
//						Person.PhoneNumber.newBuilder().setNumber("555-4321")
//								.setType(Person.PhoneType.HOME)).build();
//		AddressBook addressBook = AddressBook.newBuilder().addPerson(john)
//				.build();
//		response.setContentType("application/x-protobuf");
//		OutputStream outputStream = response.getOutputStream();
//		addressBook.writeTo(outputStream);
//		outputStream.flush();
//		outputStream.close();
//	}
//}
