package com.xgame.util;


public enum ClientType {
	SDK(0,"SDK"),YIMI(1, "一米用户"),APP(2, "app用户"),WEB(3,"web用户");

    private Integer id;

    private String name;

    private ClientType(Integer id, String name) {
        this.id=id;
        this.name=name;
    }

    public static ClientType getById(Integer id) {
        if(null==id){
            return null;
        }
        for(ClientType tmp: ClientType.values()) {
            if(tmp.id.intValue() == id.intValue()) {
                return tmp;
            }
        }
        return null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id=id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name=name;
    }
}
