package com.xgame.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;


public class ObjectJsonUtil {
	private static CustomObjectMapper mapper;
	static {
		mapper = new CustomObjectMapper();
	}
	
	 /**
     * 把对象转换为json存贮字串,不加密
	 * @param <T>
     * @param inst
     * @return
     */
    public static <T> String object2JsonString(T inst){
        if (inst == null)
            return "";
        String json = "{}";
		try {
			json = mapper.writeValueAsString(inst);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return  json;
    }
    
    
    /**
     * 转换json成对象,不解密
     * @param clazz
     * @param jsonStr
     * @return
     */
	public static Object JsonStringToObj(String jsonStr,Class<?> clazz){
        if (jsonStr == null){
            return null;
        }
        Object o=null;
		try {
			o=mapper.readValue(jsonStr,clazz);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return o;
    }
	
}
