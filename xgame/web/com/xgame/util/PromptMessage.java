package com.xgame.util;

import com.xgame.util.VM;

public class PromptMessage {

	// 用户相关
	public static final String LOGINSUCCESS="登陆成功";
	public static final String LOGINERROE="登陆超时";
	public static final  String LOGINFERO="不存在";
	public static final  String LOGINFAILNAME="用户名不存在";
	public static final  String EXISTUSERNAME="用户名已经存在";
	public static final  String LOGINFAILPASSWORD="密码错误，请重新输入";
	public static final  String LOGINFTHIRDWARN="请注意，该用户已经登陆过第三方账号";
	
	
	//效验相关
	public static final String CHECKEMAIL ="邮箱不存在";
	public static final String CKECKEMEILERROR="未绑定密保邮箱";
//	public static final String CHECKUSERNAMEERROR ="用户名不存在";
	public static final String CHECKEMAILCODEERROE ="验证码错误";
	public static final String CHECKECODEERROE ="验证码过期";
	public static final String CHECKERROE ="验证码失败";
	public static final String CHECKQUESTIONERROR ="没有设置问题";
	public static final String CHECKQUESTION ="未绑定密保问题";
	public static final String CHECKPHONE="电话没有设置";
	public static final String PASSWOEDERROE="密码答案不对";
	public static final String CKECKERROR="验证错误";
	public static final String SIXERROR=VM.getInatance().getHour()+"小时内注册次数超过限制";
	
//	积分相关
	public static final String FIRSTADDSCORE ="第一次增加积分，积分来源";
	
	
	//系统错误
	
	public static final String SYSTEMERROR="系统错误";
	
	
	//社区抽奖
	public static final String GIFTSUCCESS = "恭喜你获取礼物成功" ;
	public static final String GIFTFAILURE = "服务器繁忙，获取礼物失败" ;
	public static final String NOLOGIN     = "你没有登录,请登录!!" ;
	public static final String SCORELESS   = "你的积分不足，请获取更多的积分再来抽奖" ;
	public static final String MAKEGIFTERROR = "生成礼物失败" ;
	
	//社区游戏信息操作
	public static final String OPERATEFAILURE = "操作失败" ;
	
	//社区留言
	public static final String GETMESSAGESUCCESS = "获取留言成功" ;
	public static final String GETMESSAGEFAILURE = "获取留言失败" ;
	public static final String PUBLISHMESSAGEFAILURE = "发表留言失败" ;
	public static final String PUBLISHMESSAGESUCCESS = "发表留言成功" ;
	
	//社区获取游戏攻略
	public static final String GAMEINTRODUCTIONSUCCESS = "获取游戏攻略成功" ;
	public static final String GAMEINTRODUCTIONFAILURE = "获取游戏攻略失败" ;
	//游戏问答
	public static final String GAMEASKSUCCESS = "获取问答成功" ;
	public static final String GAMEASKFAILURE = "获取问答失败" ;
	//提问
	public static final String USERASKSUCCESS =  "发表问题成功"  ;
	public static final String USERASKFAILURE =  "服务器正忙，请稍后"  ;
	//没有登录提问
	public static final String USERASKNOLOGIN = "你还没有登录请先登录!!" ;
	//奖品仓库
	public static final String MYGIFTSUCCESS = "获取我的奖品成功" ;
	public static final String MYGIFTFAILURE = "获取我的奖品失败" ;
	
	
	//活动
	public static final String GETALLACTIVITYSUCCESS = "获取所有的活动成功" ;
	public static final String GETALLACTIVITYFAILURE = "获取所有的活动失败" ;
	//发号
    public static final String GETALLCODESUCCESS = "获取所有的发号成功" ;
	public static final String GETALLCODEFAILURE = "获取所有的发号失败" ;
	
	
}
