package com.xgame.util;

/**
 * 默认配置类
 */
public class DefaultConfig {

	// 默认编码方式

	public static final String DEFALUT_CHARSET = "UTF-8";

	// 抽奖最少花费的积分
	public static final String GIFT_SCORE = "giftScore";
	// 每天登录奖的积分
	public static final String DAY_CLIENT_SCORE = "dayClientScore";
	// 第一次登录获取的积分
	public static final String FIRST_SCORE = "firstScore";

	/*
	 * 像这些参数可以在数据库中设置默认的图片或文字
	 */
	// 系统提供的用户默认的头像（ 如果用户自己没有头像则用这个）
	public static final String DEFALUTUSERHEADER = "sns/static/img/logo.png";

	// 系统提供的用户默认的icon
	public static final String DEFALUTUSERICON = "sns/static/img/vip.png";

	/*
	 * 各个专题宣传图片的AlbumId 这些都是从数据中加载的
	 */
	// 活动专题展示图片
	public static final Integer HUODONG = 1;
	// 推荐专题展示图片
	public static final Integer TUIJIAN = 2;
	// 推荐应用
	public static final Integer TUIJIAN_APP = 3;

	// 各大版块的的接口 参数暂时省略
	public static final String APPDOWNLOAD = "page/sns/app/download?appId=";

	public static final Integer APP_CSID = 1;

	// 未登录用户本地保存的cookie
	public static final String YOULEUSER = "YOULEUSER";

	// 默认的用户头像
	public static final String headerImage1 = "sns/static/img/delete/t1.jpg";
	public static final String headerImage2 = "sns/static/img/delete/t2.jpg";
	public static final String headerImage3 = "sns/static/img/delete/t3.jpg";
	public static final String headerImage4 = "sns/static/img/delete/t4.jpg";
	public static final String headerImage5 = "sns/static/img/delete/t5.jpg";
	public static final String headerImage6 = "sns/static/img/delete/t6.jpg";
	public static String header[] = { headerImage1, headerImage2, headerImage3,
			headerImage4, headerImage5, headerImage6 };

	// 登录用户
	public static final Integer LOGINGIFT = 1;
	// 手机用户奖品 类型
	public static final Integer CLIENTGIFT = 2;

}
