package com.xgame.filter.listener;
//package com.xgame.filter.listener;
//
//import java.util.HashSet;
//
//import javax.servlet.ServletContext;
//import javax.servlet.http.HttpSession;
//import javax.servlet.http.HttpSessionEvent;
//import javax.servlet.http.HttpSessionListener;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.xgame.pojo.User;
//import com.lys.util.SpringContextUtil;
//@Service
//public class MySessionListener implements HttpSessionListener {
//	private static final Logger log = LoggerFactory.getLogger(MySessionListener.class);
//	
//	@Autowired
//	SpringContextUtil springContextUtil;
//	
//       public void sessionCreated(HttpSessionEvent event) {
//               HttpSession session = event.getSession();
//               ServletContext application = session.getServletContext();
//              // 在application范围由一个HashSet集保存所有的session
//               HashSet sessions = (HashSet) application.getAttribute("sessions");
//              if (sessions == null) {
//                      sessions = new HashSet();
//                      application.setAttribute("sessions", sessions);
//               }
//              log.info("size"+sessions.size()+"session周期:"+session.getMaxInactiveInterval());
//              
//              // 新创建的session均添加到HashSet集中
//               sessions.add(session);
//              // 可以在别处从application范围中取出sessions集合
//              // 然后使用sessions.size()获取当前活动的session数，即为“在线人数”
//        }
//
//       public void sessionDestroyed(HttpSessionEvent event) {
//               HttpSession session = event.getSession();
//               ServletContext application = session.getServletContext();
//               HashSet sessions = (HashSet) application.getAttribute("sessions");
//               User user=(User) session.getAttribute("loginUser");
//               if(user!=null){
//                log.info("size"+sessions.size()+"session周期:"+session.getMaxInactiveInterval()+",清除到期的sid："+session.getId(),"帐号:"+user.getUid());
//				user.setState(false);
//               }
//              // 销毁的session均从HashSet集中移除
//               sessions.remove(session);
//        }
//}