package com.xgame.filter;
//package com.xgame.filter;
//
//import java.io.IOException;
//
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.commons.codec.binary.Base64;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.context.support.WebApplicationContextUtils;
//
//import com.lys.market.service.ClientService;
//import com.xgame.controller.util.BaseLoginMethod;
//import com.xgame.pojo.Criteria;
//import com.xgame.pojo.User;
//import com.xgame.pojo.UserExtinfo;
//import com.xgame.service.UserExtInfoService;
//import com.xgame.service.UserService;
//import com.xgame.util.AutoLoginKeyUtil;
//import com.lys.util.XorPlus;
//
//public class AutoLoginFilter extends BaseLoginMethod implements Filter {
//
//	private XorPlus xorPlus;
//
//	@Autowired
//	ClientService clientService;
//
//	@Autowired
//	UserService userService;
//	
//	@Autowired
//	UserExtInfoService userExtInfoService ;
//
//	protected void doFilterHttp(HttpServletRequest request,
//			HttpServletResponse response, FilterChain chain) {
//		try {
//			User loginUser = getLoginUser(request, response);
//			if (loginUser == null) {
//				log.info("begin auto_cid->" + request.getSession().getId());
//				Cookie[] cookies = request.getCookies();
//				if (cookies != null) {
//					for (Cookie cookie : cookies) {
//						if ("SID".equals(cookie.getName())) {
//							String key = cookie.getValue();
//							if ((StringUtils.isNotEmpty(key))) {
//								if (Base64.isArrayByteBase64(key.getBytes())) {
//									loginUser = autoLogin(key,request);
//									if (loginUser != null)
//										break;
//								}
//							}
//						}
//					}
//				}
//				if (loginUser == null) {
//					String sid = request.getParameter("SID");
//					if (StringUtils.isNotEmpty(sid)) {
//						if (Base64.isArrayByteBase64(sid.getBytes())) {
//							loginUser = autoLogin(sid,request);
//						}
//					}
//				}
//				if (loginUser != null) {// 成功认证
//					if (getLoginUser(request, response) == null) {
//						setUserSession(loginUser, request);
//						log.info("#client_ip#" + getClientIp(request)
//								+ "login..success");
//					}
//				}
//			}
//			chain.doFilter(request, response);
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (ServletException e) {
//			e.printStackTrace();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 自动登录
//	 * 
//	 * @param key
//	 * @return
//	 */
//	private User autoLogin(String key , HttpServletRequest request) {
//		String userName = null;
//		String password = null;
//		byte[] decodeBytes = Base64.decodeBase64(key.getBytes());
//		String decodeInfo = new String(decodeBytes);
//		String[] infos = decodeInfo.split(":");
//		userName = infos[1];
//		Criteria example = new Criteria();
//		example.put("name", userName);
//		User authUser = null;
//		try {
//			authUser = userService.findUserByName(userName);
//			
//		} catch (Exception e) {
//			return null;
//		}
//		if (authUser != null) {
//			
//			password = authUser.getPassword();
//			String LKey = AutoLoginKeyUtil.getLmd5Key(userName, password);
//			if (key != null && key.equals(LKey))// 认证成功
//			{
//				UserExtinfo userExtinfo = userExtInfoService.selectByPrimaryKey(authUser.getUid());
//				request.getSession().setAttribute("USEREXTINFO", userExtinfo) ;
//				authUser.setLogin(true);
//				authUser.setRole(1) ;
//				return authUser;
//			}
//		}
//		return null;
//	}
//
//	public void destroy() {
//
//	}
//
//	// 只有在/login.html情况下才进行拦截通过，否则拦截不通过跳到登陆页面
//	private boolean pass(String urlPath) {
//		if (urlPath.indexOf("/platform/") >= 0
//				|| urlPath.indexOf("/auth/") >= 0
//				|| urlPath.indexOf("account/test") >= 0
//				|| urlPath.indexOf("game/recommend_list") >= 0
//				|| urlPath.indexOf("/pageAdv/") >= 0
//				|| urlPath.indexOf("/logout") >= 0) {
//			return true;
//		}
//		return false;
//	}
//
//	public void doFilter(ServletRequest request, ServletResponse response,
//			FilterChain chain) throws IOException, ServletException {
//		if (request instanceof HttpServletRequest
//				&& response instanceof HttpServletResponse) {
//			doFilterHttp((HttpServletRequest) request,
//					(HttpServletResponse) response, chain);
//			return;
//		}
//		chain.doFilter(request, response);
//	}
//
//	public void init(FilterConfig config) throws ServletException {
//		ApplicationContext context = WebApplicationContextUtils
//				.getWebApplicationContext(config.getServletContext());
//		context.getAutowireCapableBeanFactory().autowireBeanProperties(this,
//				AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, true);
//	}
//}