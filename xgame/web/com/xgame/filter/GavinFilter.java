package com.xgame.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GavinFilter implements Filter {
	protected final Logger log = LoggerFactory.getLogger(GavinFilter.class);
	public void destroy() {
		// TODO 自动生成方法存根
	}
	
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) resp;
		WrapperResponse wrapperResponse = new WrapperResponse(response);
        chain.doFilter(req, resp);  
	}
    public static final String arrayToString(byte[] bytes)
    {
            StringBuffer buff = new StringBuffer();
            for (int i = 0; i < bytes.length; i++)
            {
                    buff.append(bytes[i] + " ");
            }
            return buff.toString();
    }
	public void init(FilterConfig arg0) throws ServletException {
		// TODO 自动生成方法存根
	}
}