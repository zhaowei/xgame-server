package com.xgame.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.ServletContextRequestLoggingFilter;

/***
 * Request logging filter that adds the request log message to the Log4J
 * nested diagnostic context (NDC) before the request is processed,
 * removing it again after the request is processed.
 *
 * @author Juergen Hoeller
 * @author Rob Harrop
 * @since 1.2.5
 * @see #setIncludeQueryString
 * @see #setBeforeMessagePrefix
 * @see #setBeforeMessageSuffix
 * @see #setAfterMessagePrefix
 * @see #setAfterMessageSuffix
 * @see org.apache.log4j.NDC#push(String)
 * @see org.apache.log4j.NDC#pop()
 */
//public class LogContextFilter extends AbstractRequestLoggingFilter {
@Service
public class LogContextFilter extends ServletContextRequestLoggingFilter {
	/*** Logger available to subclasses */
	protected final Logger log = LoggerFactory.getLogger(LogContextFilter.class);

	@Override
	public void setIncludeClientInfo(boolean includeClientInfo) {
		// TODO Auto-generated method stub
		super.setIncludeClientInfo(includeClientInfo);
	}

	/***
	 * Logs the before-request message through Log4J and
	 * adds a message the Log4J NDC before the request is processed.
	 */
	@Override
	protected void beforeRequest(HttpServletRequest request, String message) {
//		log.info(message);
	}

	/***
	 * Removes the log message from the Log4J NDC after the request is processed
	 * and logs the after-request message through Log4J.
	 */
	@Override
	protected void afterRequest(HttpServletRequest request, String message) {
		log.debug(message);//打印
		log.info(request.getContentType());
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		super.doFilterInternal(request, response, filterChain);
	}
}
