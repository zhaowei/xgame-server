package com.xgame.util.goods;

import java.util.ArrayList;
import java.util.List;

public class TestRunnerLottery {

	public static void main(String[] args) {

		List<RunnerReward> rlist = initRewards();

		RunnerLottery lottery = new RunnerLottery();

		List<RunnerReward> rewards = null;

		for (int i = 0; i < 1000; i++) {

			rewards = lottery.getKeys(rlist, 3);

			if (isWinner(rewards)) {

				System.out.println("============================抽奖开始  第" + i
						+ "次 ===============================");

				for (RunnerReward r : rewards) {

					System.out.println(r);

				}

				System.out
						.println("============================抽奖结束===============================");

			}

		}

	}

	public static boolean isWinner(List<RunnerReward> list) {

		boolean isWinner = false;

		for (int i = 0; i < list.size(); i++) {

			for (int j = i + 1; j < list.size(); j++) {

				if (list.get(i).index != list.get(j).index) {

					return false;

				} else {

					isWinner = true;

				}

			}

		}

		return isWinner;

	}

	public static List<RunnerReward> initRewards() {

		List<RunnerReward> rlist = new ArrayList<RunnerReward>();

		rlist.add(new RunnerReward(1, "香蕉", 5));

		rlist.add(new RunnerReward(2, "苹果", 15));

		rlist.add(new RunnerReward(3, "橘子", 5));

		rlist.add(new RunnerReward(4, "葡萄", 15));

		rlist.add(new RunnerReward(5, "荔枝", 5));

		rlist.add(new RunnerReward(6, "西瓜", 5));

		rlist.add(new RunnerReward(7, "柚子", 20));

		rlist.add(new RunnerReward(8, "橙子", 10));

		rlist.add(new RunnerReward(9, "柿子", 5));

		rlist.add(new RunnerReward(10, "芒果", 15));

		return rlist;

	}

}