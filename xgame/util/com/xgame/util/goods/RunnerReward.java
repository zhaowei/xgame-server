package com.xgame.util.goods;

/**
 * 
 * @author zhaowei
 * 
 * @date 2012-10-3
 * 
 * @desc:
 * 
 *        抽奖奖品
 */

public class RunnerReward {

	/**
	 * 
	 * 奖品编号
	 */

	public int index;

	/**
	 * 
	 * 奖品名称
	 */

	public String name;

	/**
	 * 
	 * 中奖概率
	 */
	public int succPercent;

	public RunnerReward(int index, String name, int succPercent) {

		super();

		this.index = index;

		this.name = name;

		this.succPercent = succPercent;

	}

	@Override
	public String toString() {

		return "Reward [index=" + index + ", name=" + name + ", succPercent="

		+ succPercent + "]";

	}

}