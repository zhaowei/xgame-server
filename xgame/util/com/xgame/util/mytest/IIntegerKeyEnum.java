package com.xgame.util.mytest;
public interface IIntegerKeyEnum extends IEnum {
    
    public abstract Integer getIntegerKey();
}
