package com.xgame.util.mytest;
public interface IEnum {
    
    public abstract String getName();
    
    public abstract String getKey();
    
    public abstract String getValue();
}
