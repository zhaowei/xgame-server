package com.xgame.util.mytest;
public enum AcceptanceTypeEnum implements IEnum, IIntegerKeyEnum {

	/** 项目验收 */
	PROJECT_ACCEPTANCE("1", "项目验收"),
	
	/** 合同初验*/
	CONTRACT_INITIAL_TEST("2", "合同初验"),
	/** 合同终验 */
	CONTRACT_FINAL("3", "合同终验");

	/** 键 */
	private String key;

	/** 整型键值 */
	private Integer integerKey;

	/** 值 */
	private String value;

	private AcceptanceTypeEnum(String key, String value) {
		this.key = key;
		this.integerKey = Integer.valueOf(key);
		this.value = value;
	}

	public String getKey() {
		return this.key;
	}

	public String getName() {
		return this.name();
	}

	public String getValue() {
		return this.value;
	}

	public Integer getIntegerKey() {
		return integerKey;
	}
}
