package com.xgame.util.mytest;
public enum DeleteStatusEnum implements IEnum, IIntegerKeyEnum {

	/** 不可删除 */
	NO_DELETE("0", "不可删除"),
	/** 可删除*/
	MAY_DELETE("1", "可删除");

	/** 键 */
	private String key;

	/** 整型键值 */
	private Integer integerKey;

	/** 值 */
	private String value;

	private DeleteStatusEnum(String key, String value) {
		this.key = key;
		this.integerKey = Integer.valueOf(key);
		this.value = value;
	}

	public String getKey() {
		return this.key;
	}

	public String getName() {
		return this.name();
	}

	public String getValue() {
		return this.value;
	}

	public Integer getIntegerKey() {
		return integerKey;
	}
}