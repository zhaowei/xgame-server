package com.xgame.util.mytest;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


/**
 * 对枚举转换的操作
 * @author 赵巍
 * @since JDK1.7
 * @history 2013-7-23 赵巍新建
 */
public class ConvertEnum {
    
    public String printEnumValues(Class c) {
        String sb = "[";
        if (c.isEnum()) {
        	System.out.println(c.getSimpleName());
            try {
                Object[] objs = c.getEnumConstants();
                for (Object obj : objs) {
                    Method m = obj.getClass().getDeclaredMethod("values", null);
                    Object[] result = (Object[]) m.invoke(obj, null);
                    
                    // System.out.println(result);
                    
                    List list = Arrays.asList(result);
                    
                    Iterator it = list.iterator();
                    
                    while (it.hasNext()) {
                        
                        Object objOne = it.next();
                        
                        Field code = objOne.getClass().getDeclaredField("key");
                        
                        Field value = objOne.getClass().getDeclaredField("value");
                        
                        Field priority = null;
                        
                        try {
                            
                            priority = objOne.getClass().getDeclaredField("priority");
                            
                        } catch (Exception e) {
                            
                            priority = null;
                            
                        }
                        
                        code.setAccessible(true);
                        
                        value.setAccessible(true);
                        
                        if (priority != null) {
                            
                            priority.setAccessible(true);
                            
                        }
                        
                        sb += ",{text:\'" + code.get(objOne) + "\'," + value.getName() + ":\'"
                        
                        + value.get(objOne) + "\'}";
                        // System.out.print(code.getName() + ":\"" +
                        // code.get(objOne) + "\"," + value.getName() + ":"
                        //
                        // + value.get(objOne));
                        
                        // if (priority != null) {
                        //
                        // System.out.print("," + priority.getName() + ":" +
                        // priority.get(objOne));
                        //
                        // }
                        
                        // System.out.println();
                        
                    }
                    
                    break;
                    
                }
                
            } catch (Exception e) {
                
                e.printStackTrace();
                
            }
            
        }
        return sb.replaceFirst(",", "'"+c.getSimpleName()+"':") + "]";
    }
    
    public static String toAllEnums(Class... iEnums){
    	ConvertEnum t = new ConvertEnum();
    	String json="";
    	for (int i = 0; i < iEnums.length; i++) {
    		json +=",\""+t.printEnumValues(iEnums[i])+"\"";
		}
		return "["+json.replaceFirst(",", "")+"]";
    }
    
    public static void main(String[] args) {
        ConvertEnum t = new ConvertEnum();
//        String s = "'AcceptanceConclusionEnum':" + t.printEnumValues(AcceptanceTypeEnum.class);
        String s =  t.printEnumValues(AcceptanceTypeEnum.class);
        List<String> list=new ArrayList<>();
        list.add(s);
        list.add(s);
        //往这个里面添加枚举类型即可
        Class[] cs={AcceptanceTypeEnum.class,DeleteStatusEnum.class};
		System.out.println("所有的枚举:"+toAllEnums(cs));
//        System.out.println(s);
//        String json=ObjectJsonUtil.object2JsonString(list);
//        System.out.println(json);
    }
    
    
    
}

