package com.xgame.util.cache;

import java.util.Collection;

import org.springframework.cache.Cache;
import org.springframework.cache.support.AbstractCacheManager;

import com.danga.MemCached.MemCachedClient;

public class MemcacheCacheManager extends AbstractCacheManager {
  
  private Collection<Cache> caches;
  private MemCachedClient client = null;
  
  public MemcacheCacheManager() {

  }
  
  public MemcacheCacheManager(MemCachedClient client){
    setClient(client);
  }
  
  @Override
  protected Collection<? extends Cache> loadCaches() {    
    return this.caches;
  }
  
  public void setCaches(Collection<Cache> caches) {
    this.caches = caches;
  }
  
  public void setClient(MemCachedClient client) {
    this.client = client;
    updateCaches();
  }
  
  public Cache getCache(String name){
    checkState();
    
    Cache cache = super.getCache(name);
    if(cache == null){
      cache = new MemcacheCache(name, client);
      addCache(cache);
    }
    return cache;
  }
  
  private void checkState() {
    if(client == null){
      throw new IllegalStateException("MemcacheClient must not be null.");
    }
    //TODO check memcache state
    
  }

  private void updateCaches() {
    if(caches != null){
      for(Cache cache : caches){
        if(cache instanceof MemcacheCache){
          MemcacheCache memcacheCache = (MemcacheCache)cache;
          memcacheCache.setClient(client);
        }
      }
    }
    
  }
   
}