package com.xgame.util.cache;

import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.util.Assert;

import com.danga.MemCached.MemCachedClient;

public class MemcacheCache implements Cache {
  
  private MemCachedClient client;
  private String name;
  
  public MemcacheCache(){
    
  }
  
  public MemcacheCache(String name,MemCachedClient client) {
    Assert.notNull(client, "Memcache client must not be null");
    // TODO validate memcache "alive"
    this.client = client;
    this.name = name;
  }
  
  @Override
  public String getName() {
    return this.name;
  }
  
  @Override
  public Object getNativeCache() {
    return this.client;
  }
  
  @Override
  public ValueWrapper get(Object key) {
    Object value = this.client.get(objectToString(key));
    return (value != null ? new SimpleValueWrapper(value) : null);
  }
  
  @Override
  public void put(Object key, Object value) {
    this.client.set(objectToString(key), value);
    
  }
  
  @Override
  public void evict(Object key) {
    this.client.delete(objectToString(key));
    
  }
  
  @Override
  public void clear() {
    // TODO delete all data   
	  client.flushAll();
  }
  
  private static String objectToString(Object object) {
    if (object == null) {
      return null;
    } else if (object instanceof String) {
      return (String) object;
    } else {
      return object.toString();
    }
  }
  
  public void setClient(MemCachedClient client){
    this.client = client;
  }

  public MemCachedClient getClient() {
    return client;
  }

  public void setName(String name) {
    this.name = name;
  }
    
}
