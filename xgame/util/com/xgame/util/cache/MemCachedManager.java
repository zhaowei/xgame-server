package com.xgame.util.cache; 
 
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.danga.MemCached.MemCachedClient;
import com.xgame.memcache.ICacheClient;
 
@Service
public class MemCachedManager { 
	@Autowired
	ICacheClient memcache; 
    /*// 创建全局的唯一实例   
     
    /**  
     * 保护型构造方法，不允许实例化！ */   
    protected MemCachedManager() {   
    } 
     
//    /**  
//     * 获取唯一实例 */   
//    public static MemCachedManager getInstance() {   
//        return memCachedManager;   
//    } 
     
    /**  
     * 添加一个指定的值到缓存中.  
     * @param key  
     * @param value  
     * @return */   
    public boolean add(String key, Object value) {   
        return memcache.add(key, value);   
    }   
     
    public boolean add(String key, Object value, Date expiry) {   
        return memcache.add(key, value, expiry);   
    }   
     
    public boolean replace(String key, Object value) {   
        return memcache.replace(key, value);   
    }   
     
    public boolean replace(String key, Object value, Date expiry) {   
        return memcache.replace(key, value, expiry);   
    } 
     
    /**  
     * 根据指定的关键字获取对象.  
     * @param key  
     * @return */   
    public Object get(String key) {   
        return memcache.get(key);   
    } 
     
    public static void main(String[] args) {   
//        MemCachedManager cache = MemCachedManager.getInstance();   
//        cache.add("hello", 234);
    	
//        System.out.print("get value : " + cache.get("micMsgScore2"));   
    }  
} 