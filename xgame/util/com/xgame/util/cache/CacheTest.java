package com.xgame.util.cache;
//package com.xgame.util.cache;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//
//import com.xgame.memcache.ICacheClient;
//import com.xgame.pojo.SysRule;
//import com.xgame.service.CacheService;
//import com.xgame.service.SysRuleService;
//import com.xgame.service.UserService;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@ContextConfiguration(locations = { 
//		"classpath:spring/application-mail.xml",
//		"classpath:spring/spring*.xml",
//		})
//public class CacheTest {
//	protected final Logger log = LoggerFactory.getLogger(getClass());
//	
//	@Autowired
//	UserService userService;
//	
//	@Autowired
//	SysRuleService sysRuleService;
//	
//	@Autowired
//	ICacheClient memcache;
//	
////	@Autowired
////	CacheService cacheService;
//	
//	@Test 
//	public void test(){
//		try {
//			log.info("测试缓存开始");
//			SysRule record=new SysRule();
//			record.setRuleName("micMsgScore2");
//			record.setRuleValue("300");
//			sysRuleService.updateByPrimaryKey(record);
//			record=sysRuleService.selectByPrimaryKey("micMsgScore2");
//			log.info("从cache加载：");
//			record=sysRuleService.selectByPrimaryKey("micMsgScore2");
//			
//			log.info("cache管理");
////			cacheService.flushSimpleCache("simple") ;
//			record=sysRuleService.selectByPrimaryKey("micMsgScore2");
////			log.info("从数据库加载：");
////			SysRule record2=sysRuleService.selectByPrimaryKey("micMsgScore2");
////			log.info("从cache 加载：");
////			record2=sysRuleService.selectByPrimaryKey("micMsgScore");
////			log.info(record2.getRuleValue());
////			record.setRuleValue("300");
////			log.info("再次更新cache：");
////			sysRuleService.updateByPrimaryKey(record);
////			log.info("再次从cache加载：");
////			SysRule record3=sysRuleService.selectByPrimaryKey("micMsgScore");
////			System.out.println(record3);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
////		User user=userService.findUserByName(""); 
//	}
//
////	@Test
//	public void memcacheTest(){
//		 System.out.print("get value : " + memcache.get("micMsgScore2")); 
//	}
//	
////	public static void main(String[] args) {
////	Account account = s.getAccountByName("someone"); 
////	   account.setPassword("123"); 
////	   s.updateAccount(account); 
////	   account.setPassword("321"); 
////	   s.updateAccount(account); 
////	   account = s.getAccountByName("someone"); 
////	   System.out.println(account.getPassword()); 
////	}
//}
//
