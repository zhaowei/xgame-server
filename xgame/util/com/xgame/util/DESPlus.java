﻿package com.xgame.util;

import java.net.URLEncoder;
import java.security.Key;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpUtils;

import org.springframework.stereotype.Service;

//import com.sun.crypto.provider.SunJCE;

/**
 * DES加密工具
 * 
 * @author zhaowei
 * 
 */
@Service
public class DESPlus
{
	private static String strDefaultKey = "ym";

	private Cipher encryptCipher = null;

	private Cipher decryptCipher = null;

	public DESPlus() throws Exception 
	{
		this(strDefaultKey);
	}

	public DESPlus(String strKey) throws Exception
	{
//		Security.addProvider(new SunJCE());
		Key key = getKey(strKey.getBytes());

		encryptCipher = Cipher.getInstance("DES");
		encryptCipher.init(Cipher.ENCRYPT_MODE, key);

		decryptCipher = Cipher.getInstance("DES");
		decryptCipher.init(Cipher.DECRYPT_MODE, key);
	}

	private Key getKey(byte[] arrBTmp) throws Exception
	{
		byte[] arrB = new byte[8];
		for (int i = 0; i < arrBTmp.length && i < arrB.length; i++)
		{
			arrB[i] = arrBTmp[i];
		}
		Key key = new SecretKeySpec(arrB, "DES");
		return key;
	}

	public String byteArr2HexStr(byte[] arrB) throws Exception
	{
		int iLen = arrB.length;
		StringBuffer sb = new StringBuffer(iLen * 2);
		for (int i = 0; i < iLen; i++)
		{
			int intTmp = arrB[i];
			while (intTmp < 0)
			{
				intTmp = intTmp + 256;
			}
			if (intTmp < 16)
			{
				sb.append("0");
			}
			sb.append(Integer.toString(intTmp, 16));
		}
		return sb.toString();
	}

	public byte[] hexStr2ByteArr(String strIn) throws Exception
	{
		byte[] arrB = strIn.getBytes();
		int iLen = arrB.length;
		byte[] arrOut = new byte[iLen / 2];
		for (int i = 0; i < iLen; i = i + 2)
		{
			String strTmp = new String(arrB, i, 2);
			arrOut[i / 2] = (byte) Integer.parseInt(strTmp, 16);
		}
		return arrOut;
	}

	/**
	 * DES加密
	 * 
	 * @param strIn
	 * @return
	 * @throws Exception
	 */
	public String encrypt(String strIn) throws Exception
	{
		return byteArr2HexStr(encrypt(strIn.getBytes()));
	}

	/**
	 * DES加密
	 * 
	 * @param arrB
	 * @return
	 * @throws Exception
	 */
	public byte[] encrypt(byte[] arrB) throws Exception
	{
		return encryptCipher.doFinal(arrB);
	}

	/**
	 * 解密
	 * 
	 * @param strIn
	 * @return
	 * @throws Exception
	 */
	public String decrypt(String strIn) throws Exception
	{
		return new String(decrypt(hexStr2ByteArr(strIn)));
	}

	/**
	 * 解密
	 * @param arrB
	 * @return
	 * @throws Exception
	 */
	public byte[] decrypt(byte[] arrB) throws Exception
	{
		return decryptCipher.doFinal(arrB);
	}
	
	public static void main(String[] args) {
		try {
			
			String LKEY=new DESPlus("ym").encrypt("中文的的饿阿萨德");
//			String LKEY=new DESPlus("ym").decrypt("764f95b470c9e43baeb82ff4fa6e174154b62b7e293b0bca869d75f19e22ebc454b071b808a899fa654690fd7ac3beab5e30065b884647b4c497a8e5e28e0e285e30065b884647b405ee296e53c83d40fdaf9203b0d11473546c563c6ba9f60595b3d083b64c8bb7c7db8b98b8cdaad4d34d2218543959c32c89859134e3f4ec2ad56988ba29cf19216583ce6a1c0fccddc791a655506dec940a603da8b5c9ddb28bf22e72b61e767fa9725c8908550f83425106d51d1151fd7b2c205ff8ab277f2c981fcd8773c596302b4fe96d4854db8888daa6ad856402e8efe1888b73dbaa3d83d6725c04649221aa9c82e839645cc32306dbef80443412516230798973d85e83283c39ff148c5bcd2370a04a88363bffb192416838b7eefcee985b95e5feb63d76fda40370b7eefcee985b95e534d1c2d7fdca7f063506fadaff4e75f6e2cc77bc1bbe86e800a1485f5dcde92e0f49f49a4197177d2aab5d1802bb4c6f873170cdfd71a0296cfe9e9efe0d2babb0c79e4823d277f0a716b3d3cd9a21cf63486af92e74a819fe8599ee28ec823891cc538391288084564bcdcc670ff144c80ea22f7ae8bfb85cb4d0459a40f326be78a8d664b6e24522b907b43e6ddb3307e71a5455ba96765cb4d0459a40f326413c510e4dd67c1763046c6e62ec76b9a086a31264f5b0aef1f1d9e8f0de59b0633ac07eb3a247c802c2ca3440cb63ef3f5639a4002acd692d987441cea53941c129db295878e2cf23505a1aaa3310d7dac845561d351386a5d0ccb752d544e823505a1aaa3310d7ec9f1f31f79cfe2ee94fc8cf97739bb16369d3fc1de10c79");
//			String LKEY=new DESPlus("ym").decrypt("ebee978bf1f95eacd0923c0bb62a5df8be8554acf81896d3eb5796927378d7a39c3b26f5ab6087b29f2a1c6ab22a4c7a99bbe0cd535f8eeaf2cc95e31dafdff300a1485f5dcde92e90693cd0988ecf28e9eaaea88be30a95a3f5cc07a8e5d41535bebd418ab5ec95d5fc9279e42c9c3038c119d1df0b9e869d6417fe2c8f24a980783f7a97cca14a1c625988b773d89aaf63c4352ba716a42db7247f9be12565d3024b000ffcd5f343b35a792a8e6891dda25c5ed37434d98f5fda22d01827c2d4249f288f4f7affd3d66c547479ba94609a70e670f0277f719251b1ff39bba2b00c484c5438d6540f2465d3579d0a8ce330ee1d89b964665ca383b65289edb6ec0e1283346493b2868d566246f59c6c781bf8345d6ef002b82b6e1f85a70ec0110f93c431461bc9ff1ea9dc44b4dc73521411bcb1b8abea47d5f1356c288200996dadbf899df41abffe02ac68714b028f286e2eed4e1e9be37112a56a13bb78285cd13c0d2d35b2892b70bfef51468805e8e62e02ab79abfe7da16766218092321b58782e9f36fa05e8e62e02ab79abc7372b93a2ac077011f6722abd98398b1c34676523cd52b4ff13766d2138a915b2272e902f140102bfd86401847c61c884c6add9e19e5d96abae75a50809ee0c041fa263613c1b6a0acb5aa4fe6e230f71da57fc81e38612d5461334d09b67939967692815204bed3dda1d6c99085c6b29b508312e9c62c3e2c4b7edb9a7179b34701ea47c870d1d8b7ed9b92f78b3e77d343ae631c93c96ebd3b3061adabc5896302b4fe96d4854a9965957f458818b09a33e1926561f5a9221aa9c82e83964b643062b8276874b5b7c2b2d431ffe3d852bf5e4353cddbf1187c345f95ce48673b52dc1d792560595b3d083b64c8bb758fb5042204f403c751f078f5db0b39dd7a80690e8c9fb256a77bff569079449d1db79c58099b8b9189e4d72436435c32bd30c2ec929558d32594d151859434f759c8e20d1d5fe94193a91b17ff1f781cfca2ccf6f812bb07576cf08aecc15a1b37ed4985362366eac77dbcea0e84a3cbe78a8d664b6e245c574e278380c94f8e71a55b593109930413c510e4dd67c1769b95f77c12f27928cfa4f4c4f1aaf0f5c80ec33ce9c323199bbe0cd535f8eeaf2cc95e31dafdff300a1485f5dcde92e90693cd0988ecf28bed5dc72510d22963c312dd90293bb0d35bebd418ab5ec95d5fc9279e42c9c3038c119d1df0b9e869d6417fe2c8f24a980783f7a97cca14a1c625988b773d89aaf63c4352ba716a42db7247f9be12565d3024b000ffcd5f343b35a792a8e6891dda25c5ed37434d98f5fda22d01827c2d4249f288f4f7affd3d66c547479ba94609a70e670f0277f719251b1ff39bba2b00c484c5438d6540f2465d3579d0a8ce330ee1d89b964665ca383b65289edb6ec0e1283346493b2868d566246f59c6c51f1a6f2de3a36793575644e9d206461110f93c431461bc9ff1ea9dc44b4dc73521411bcb1b8abea47d5f1356c288200996dadbf899df41abffe02ac68714b028f286e2eed4e1e9be37112a56a13bb78285cd13c0d2d35b2892b70bfef51468805e8e62e02ab79abfe7da16766218092321b58782e9f36fa05e8e62e02ab79abc7372b93a2ac077011f6722abd98398b68de5a85763c53e42ece3b7d545fa0992a09dea2696790453b98b021f307ca283011c642fc9acabf6243ad24fe3bf51fbf71bb4836f909f8972ca01a4d19e79346182551270e4dceaf67bcc57b8fb7d6");
			System.out.println(LKEY);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}