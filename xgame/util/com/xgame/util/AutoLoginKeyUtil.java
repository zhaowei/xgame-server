package com.xgame.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

public class AutoLoginKeyUtil{
	

	
	/**
	 * 
	 * @param password
	 * @return
	 * @deprecated 由于安全性太弱，将废弃该自动登录机制
	 */
	public static String getLKeyOld(String password){
		return new String(Base64.encodeBase64(DigestUtils.md5(password)));
	}
	
	/**
	 * 生成自动登录的LKey
	 * @param username
	 * @param password
	 * @return
	 */
	public static String getLKey(String username,String password){
		String pkey=DigestUtils.md5Hex(password);
		return new String(Base64.encodeBase64((pkey+":"+username).getBytes()));
	}
	
	
	/**
	 * 若密码已经加密的
	 */
	public static String getLKeyPassWordIsMD5(String username ,String password){
		return new String(Base64.encodeBase64((password+":"+username).getBytes()));
	}
	
	/**
	 * 生成自动登录的LKey
	 * @param username
	 * @param password
	 * @return
	 */
	public static String getLmd5Key(String username,String pkey){
		return new String(Base64.encodeBase64((pkey+":"+username).getBytes()));
	}
	
	//测试
	public static void main(String[] args){
		String username = "mike88" ;
		String password = "123456" ;
		String md5Password = "e10adc3949ba59abbe56e057f20f883e" ;
		System.out.println(getLKey(username, password)) ;
		System.out.println(getLKeyPassWordIsMD5(username, md5Password)) ;
	}
}
