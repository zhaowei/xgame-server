package com.xgame.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 工具类
 * 
 * @author thj
 */
public class UserUtil {

	/**
	 * 判断字符串是否为空 若为空串则返回true否则返回false
	 */
	public static boolean isBank(String str) {
		if (str != null && !str.equals("")) {
			return false;
		}
		return true;
	}

	/**
	 * 生成随机密码长度为8位
	 */
	public static String randomPassWord() {
		char[] c = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
				'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
				'x', 'y', 'z' };
		StringBuffer pass = new StringBuffer();
		Random random = new Random();
		for (int i = 0; i < 8; i++) {
			pass.append(c[random.nextInt(24)]);
		}
		return pass.toString();
	}

	/**
	 * 生成随机账号 从注册当前时间到2013年5月13号时间毫秒值
	 */
	public static String randomAccount() {
		String str = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date1 = null;
		try {
			date1 = format.parse("2013-5-13 12:24:24");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date date = new Date();
		long time = date.getTime() - date1.getTime();
		// 转化为16进制
		str = Integer.toHexString((int) time);
		return "y" + str;
	}

	/**
	 * 将str拆分成map(cpId,clientId,appId,channelId) String info = "" + appId + "&"
	 * + channelId + "&" + cpId + "&" + clientId;
	 */
	public static Map<String, String> getCookieValue(String str) {
		Map<String, String> map = new HashMap<String, String>();
		String[] s = str.split("&");
		map.put("appId", s[0]);
		map.put("channelId", s[1]);
		map.put("cpId", s[2]);
		map.put("clientId", s[3]);
		return map;
	}
}
