package com.xgame.util;


import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.xgame.pojo.base.BaseBean;

public class VM extends BaseBean
{
	private final Logger log = LoggerFactory.getLogger(VM.class);
	
	Resource resource = new ClassPathResource("conf//vm.properties");

	Resource diffServerResource = new ClassPathResource("conf//diffserver.properties");

	public static long checkTime;

	public long getCheckTime()
	{
		return checkTime;
	}

	public Resource getResource()
	{
		return resource;
	}

	private Properties vmConfig;

	private Properties diffServerConfig;

	private Properties commonVarConfig;

	private static VM manager;

	private String webContextPath;// 应用的根目录

	private String maxUseSize;

	private String webUrl;

	private String imgServer;

	private String apkServer;

	private String passLoginUrl;

	private String resServer;
	
	private String activeUrl ;
	
	private String serviceUrl ;//游乐客服电话
	
	private Integer isActive ;
	
	private String hostPayUrl;
	
	private String lypay;
	
	private String feepayTransUrl;
	
	private String alipay;

	private String pay;
	
	private boolean signBoolean;
	
	private String upgrade;
	
	private String staticServer;
	
	private String sdkServer;
	
	private String userServer;
	
	private String emailAddress;
	
	private int hour;
	
	private int number;
	
	//系统规则
	private String dayClientScore ;
	private String firstScore ;
	private String giftScore ;
	
	
	public String getDayClientScore() {
		return dayClientScore;
	}

	public void setDayClientScore(String dayClientScore) {
		this.dayClientScore = dayClientScore;
	}

	public String getFirstScore() {
		return firstScore;
	}

	public void setFirstScore(String firstScore) {
		this.firstScore = firstScore;
	}

	public String getGiftScore() {
		return giftScore;
	}

	public void setGiftScore(String giftScore) {
		this.giftScore = giftScore;
	}

	public String getUpgrade() {
		return upgrade;
	}

	public void setUpgrade(String upgrade) {
		this.upgrade = upgrade;
	}

	public boolean isSignBoolean() {
		return signBoolean;
	}

	public void setSignBoolean(boolean signBoolean) {
		this.signBoolean = signBoolean;
	}

	public String getPay() {
		return pay;
	}

	public void setPay(String pay) {
		this.pay = pay;
	}

	public String getAlipay() {
		return alipay;
	}

	public void setAlipay(String alipay) {
		this.alipay = alipay;
	}

	public String getFeepayTransUrl() {
		return feepayTransUrl;
	}

	public void setFeepayTransUrl(String feepayTransUrl) {
		this.feepayTransUrl = feepayTransUrl;
	}

	public String getLypay() {
		return lypay;
	}

	public void setLypay(String lypay) {
		this.lypay = lypay;
	}

	public String getHostPayUrl() {
		return hostPayUrl;
	}

	public void setHostPayUrl(String hostPayUrl) {
		this.hostPayUrl = hostPayUrl;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	public String getActiveUrl() {
		return activeUrl;
	}

	public void setActiveUrl(String activeUrl) {
		this.activeUrl = activeUrl;
	}

	private static String[] cacheURLs;

	public static String[] getCacheURLs()
	{
		return cacheURLs;
	}

	public String getPassLoginUrl()
	{
		return passLoginUrl;
	}

	public void setPassLoginUrl(String passLoginUrl)
	{
		this.passLoginUrl = passLoginUrl;
	}

	private VM(String wcp)
	{
		init(wcp);
	}

	public VM()
	{
	}

	public String getWebContextPath()
	{
		return webContextPath;
	}

	public void setWebContextPath(String webContextPath)
	{
		this.webContextPath = webContextPath;
	}

	public String getResServer() {
		return resServer;
	}

	public void setResServer(String resServer) {
		this.resServer = resServer;
	}

	public String getApkServer()
	{
		return apkServer;
	}

	public void setApkServer(String apkServer)
	{
		this.apkServer = apkServer;
	}

	public String getImgServer()
	{
		return imgServer;
	}

	public void setImgServer(String imgServer)
	{
		this.imgServer = imgServer;
	}

	public String getWebUrl()
	{
		return webUrl;
	}

	public void setWebUrl(String webUrl)
	{
		this.webUrl = webUrl;
	}

	public String getMaxUseSize()
	{
		return maxUseSize;
	}

	public void setMaxUseSize(String maxUseSize)
	{
		this.maxUseSize = maxUseSize;
	}
	
	public String getStaticServer() {
		return staticServer;
	}

	public void setStaticServer(String staticServer) {
		this.staticServer = staticServer;
	}

	public String getSdkServer() {
		return sdkServer;
	}

	public void setSdkServer(String sdkServer) {
		this.sdkServer = sdkServer;
	}

	public String getUserServer() {
		return userServer;
	}

	public void setUserServer(String userServer) {
		this.userServer = userServer;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	/**
	 * 重新初始化
	 * 
	 * @author zhaowei
	 * @return void
	 * @date 2011-8-23 上午10:23:05
	 * @since 1.0.0
	 */
	public void reInit()
	{
		vmConfig = new Properties();
		try
		{
			// InputStream is = VM.class.getClassLoader().getResourceAsStream("vm.properties");
			// Resource resource = new ClassPathResource("vm.properties");
			// InputStream is = resource.getInputStream();
			InputStream is = new FileInputStream(resource.getFile());
			// InputStream is = VM.class.getResource("/vm.properties");
			vmConfig.load(is);
			checkTime = System.currentTimeMillis();
		}
		catch (Exception e)
		{
			log.error(e.getLocalizedMessage(), e);
		}
		try
		{
			diffServerConfig = new Properties();
			InputStream is = new FileInputStream(diffServerResource.getFile());
			diffServerConfig.load(is);
		}
		catch (Exception e)
		{
			log.error(e.getLocalizedMessage(), e);
		}
		staticServer=getProperty("staticServer");
		sdkServer=getProperty("sdkServer");
		userServer=getProperty("userServer");
		resServer = getProperty("resServer");
		emailAddress = getProperty("emailAddress");
		number = getIntProperty("number");
		hour = getIntProperty("hour");
		lypay=getProperty("lypay");
		dayClientScore = getProperty("dayClientScore");
		firstScore = getProperty("firstScore");
		giftScore  = getProperty("giftScore");
		if (log.isInfoEnabled())
		{
			log.info("VM装载完成");
		}
	}

	public void init(String wcp)
	{
		reInit();
		webContextPath = wcp;
	}

	public synchronized static VM getInatance(String wcp)
	{
		if (manager == null)
		{
			manager = new VM(wcp);
		}
		return manager;
	}

	// ----------------------------------
	public synchronized static VM getInatance()
	{
		if (manager == null)
		{
			manager = new VM();
		}
		return manager;
	}

	public String getProperty(String key)
	{
		String result = vmConfig.getProperty(key);
		if (StringUtils.isEmpty(result))
		{
			 return this.getPropertyByDiffServer(key);
		}else
			log.info("VM提取属性 [" + key + "=" + vmConfig.getProperty(key) + "]");
		return vmConfig.getProperty(key).trim();
	}

	public boolean getBooleanProperty(String name)
	{
		// get the value first, then convert
		String value = getProperty(name);

		if (value == null)
			return false;

		return (new Boolean(value)).booleanValue();
	}

	public int getIntProperty(String name)
	{
		String value = getProperty(name);
		try
		{
			return Integer.parseInt(value);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		return 0;
	}

	public String getPropertyByDiffServer(String key)
	{
		log.info("DiffServer提取属性 [" + key + "=" + diffServerConfig.getProperty(key) + "]");
		String val = diffServerConfig.getProperty(key);
		if (val == null)
		{
			return "";
		}
		return val.trim();
	}

	public boolean getBooleanPropertyByDiffServer(String name)
	{
		String value = getPropertyByDiffServer(name);
		if (value == null)
			return false;

		return (new Boolean(value)).booleanValue();
	}

	public int getNumberPropertyByDiffServer(String name)
	{
		// get the value first, then convert
		String value = getPropertyByDiffServer(name);
		return Integer.parseInt(value);
	}

	public String getServiceUrl() {
		return serviceUrl;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
}
