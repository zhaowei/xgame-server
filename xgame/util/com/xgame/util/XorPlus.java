package com.xgame.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;

@Service
public class XorPlus {
	
	public XorPlus() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public static void main(String args[]) throws IOException {
		// BufferedReader stdin=new BufferedReader(new
		// InputStreamReader(System.in));
//		System.out.print("请输入加密内容：");
		String mingwen = Base64.encodeBase64String("加密这个".getBytes());
		// String secret="天行健君自强不息";
//		String secret = "yimilogkey";
//		char str[] = mingwen.toCharArray();
//		int k;
//		for (k = 0; k < str.length; k++) {
//			str[k] = (char) (str[k] ^ secret.charAt(k % 8));
//		}
//
//		String s = new String(str);
//		System.out.println("暗文：" + s);
//
//		for (k = 0; k < str.length; k++) {
//			str[k] = (char) (str[k] ^ secret.charAt(k % 8));
//		}
//		String t = new String(str);
//		System.out.print("明文：" + Base64.decodeBase64(t.getBytes()));
	}

	String secret = "y6lyuser";

	
	public String encrypt(String mingwen) {
		char str[] = mingwen.toCharArray();
		int k;
		for (k = 0; k < str.length; k++) {
			str[k] = (char) (str[k] ^ secret.charAt(k % 8));
		}
		return new String(str);
	}

	public String decrypt(String miwen) {
		char str[] = miwen.toCharArray();
		for (int k = 0; k < str.length; k++) {
			str[k] = (char) (str[k] ^ secret.charAt(k % 8));
		}
		String t = new String(str);
		return t;
	}

	public String inputStreamDecrypt(InputStream is) {
		BufferedReader in = new BufferedReader(new InputStreamReader(is));
		StringBuffer buffer = new StringBuffer();
		String line = "";
		try {
			while ((line = in.readLine()) != null) {
				buffer.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return decrypt(buffer.toString());
	}
	



}