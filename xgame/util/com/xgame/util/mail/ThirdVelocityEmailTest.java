package com.xgame.util.mail;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ThirdVelocityEmailTest {

	@Test
	public void sendEmail() {
		try {
			ApplicationContext context = new ClassPathXmlApplicationContext(
					"spring/*.xml");
			ThirdVelocityEmailService tves = context
					.getBean(ThirdVelocityEmailService.class);
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("userName", "windy@y6.cn");
			// model.put("emailAddress", "136122085@163.com");
			model.put("emailAddress", "windy@y6.cn");
			// tves.sendEmail(model,"欢迎您的加入","com/guan/chapter19/email/welcome.vm",new
			// String[]{"woshiguanxinquan@163.com"},new
			// String[]{"F:/Sunset.jpg","F:/spring-hibernate.rar"});
			tves.sendEmail(model, "欢迎您的加入",
					"com/lys/user/util/mail/Welcome.vm",
					new String[] { "136122085@163.com" }, new String[] {});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 参数说明：替换velocity模板的变量和值对，邮件主题，velocity模板文件的路径，接收方email地址，附件
		// 简单说明，如果您要群发，可以在接收方email地址中多传入几个email地址，附件可以一次发送多个
	}
}